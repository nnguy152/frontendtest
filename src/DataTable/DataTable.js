import React, { Component } from 'react'
import ReactTable from 'react-table'
import 'react-table/react-table.css'

class DataTable extends Component {
  constructor(){
    super()
    this.state = {
      image: '', // to set image url to display overlay
      active: false // for toggling image overlay
    }
    this.toggleOverlay = this.toggleOverlay.bind(this)
  }

  toggleOverlay (e) {
    // sets state of image based on target url
    // on click, returns active back to false to change style to display none
    this.setState({image: e.target.value}, () => {
      this.setState((prevState) => { return { active: !prevState.active }}
    )
  })}

   render() {
    // render each data on screen
    let data = this.props.data
    
      return (
         <div>
           {/* if active === true => displays block, div click changes state.active to false and style back to display none */}
            <div onClick={this.toggleOverlay} style={{display: this.state.active ? "block" : "none" }} className='image-container'>
              <img className='image' src={this.state.image} />
            </div>

            <ReactTable 
               data = {data}
               className="-highlight table"

               columns={[
                  { Header: 'Title', id: 'title', accessor: (data => data.title === '' || data.title === ' ' ? 'Untitled' : data.title) }, // shows Untitled for untitled items rather than blank space
                  { Header: 'Date Taken', accessor: 'date_taken', width: 120 },
                  { Header: 'Date Published', accessor: 'published', width: 120 },
                  { Header: 'Author', id: 'author', accessor: (data => data.author.split('("').pop().split('")')), width: 150 }, // gets rid of the nobody@flickr around author name
                  { Header: 'Description', accessor: 'description' },
                  { Header: 'Image', id: 'media.m', accessor: data => <button onClick={this.toggleOverlay} value={`${data.media.m}`}>Image Preview</button>, width: 100 },
                  { Header: 'Tags', id: 'tags', accessor: (data => data.tags ? data.tags : 'no tags') } // shows No tags for blank tags rather than blank space
               ]}
            />
         </div>
      );
   }
}

export default DataTable