import React from 'react'

const TagSearch = (props) => {
   return (
      <div className='search'>
         <p>Results Including <button onClick={props.toggleTagMode} >{props.tagMode.toUpperCase()}</button> Tags</p>
         <form onSubmit={props.handleTagSearch}>
            <input className='tag-input' type='text' name='tag' placeholder='Search Tags' onChange={props.handleInput} />
            <input className='submit-btn' type='submit' value='Search' />
         </form>
      </div>
   )
}

export default TagSearch