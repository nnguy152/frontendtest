import React from 'react'
import TagSearch from './TagSearch'

const Header = (props) => {
      return (
         <div className='header'>
            <div className='flickr'>
               flickr.slalom
            </div>
            <TagSearch handleInput={props.handleInput} handleTagSearch={props.handleTagSearch} toggleTagMode={props.toggleTagMode} tagMode={props.tagMode} />
         </div>
      )
}

export default Header