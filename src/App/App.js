import React, { Component } from 'react'
import './App.css'
import axios from 'axios'
import Header from '../Header/Header'
import DataTable from '../DataTable/DataTable'

class App extends Component {
  constructor () {
    super()
    this.state = {
      tag: '',
      tagMode: 'all',
      toggleTag: true,
      data: []
    }
    this.toggleTagMode = this.toggleTagMode.bind(this)
    this.handleTagSearch = this.handleTagSearch.bind(this)
    this.handleInput = this.handleInput.bind(this)
  }

  // axios call to api when page loads
  componentDidMount () {
    axios.get(`https://api.flickr.com/services/feeds/photos_public.gne?&tagmode=all&format=json&nojsoncallback=1`) // $nojsoncallback=1 tag needed to get rid of jsonFlickerFeed words
    .then(flickr => {
      this.setState({data: flickr.data.items})
    })
    .catch(error => console.log(error))
  }

  // users gets results either w/ all inputted tags or including any tag
  toggleTagMode(e) {
    this.setState((prevState) => { 
      return { toggleTag: !prevState.toggleTag }
    }, () => {
      this.state.toggleTag ? this.setState({tagMode: 'all'}) : this.setState({tagMode: 'any'})
    })
  }

  // needs ',+' to do individual word searches
  handleInput(e) {
    e.preventDefault()
    let tag = e.target.value.split(' ').join(',+') + `',+'${e.target.value}`
    this.setState({[e.target.name]: tag}, () => { this.handleTagSearch })
  }

  // on user search submit, sets tag state and does axios call to get new data based on tag
  handleTagSearch(e) {
    e.preventDefault()
    axios.get(`https://api.flickr.com/services/feeds/photos_public.gne?tags=${this.state.tag}&tagmode=${this.state.tagMode}&format=json&nojsoncallback=1`) // $nojsoncallback=1 tag needed to get rid of jsonFlickerFeed words
    .then(flickr => {
      console.log(flickr.data)
      this.setState({data: flickr.data.items})
    })
    .catch(error => console.log(error))
  }

  render() {
    return (
      <div>
        <Header handleInput={this.handleInput} handleTagSearch={this.handleTagSearch} toggleTagMode={this.toggleTagMode} tagMode={this.state.tagMode} />
        <DataTable images={this.state.images} data={this.state.data} />
      </div>
    );
  }
}

export default App
